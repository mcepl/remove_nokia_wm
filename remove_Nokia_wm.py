#!/usr/bin/env python3
# https://docs.opencv.org/master/df/d3d/tutorial_py_inpainting.html
# https://docs.opencv.org/master/db/df6/tutorial_erosion_dilatation.html
'''
Inpainting sample.

Inpainting repairs damage to images by floodfilling
the damage with surrounding image areas.

Usage:
  inpaint.py [<image>]

Keys:
  SPACE - inpaint
  r     - reset the inpainting mask
  ESC   - exit
'''

import os.path
import sys

import numpy as np
import cv2 as cv

DIR = os.path.dirname(os.path.realpath(__file__))
MASK_POR = os.path.join(DIR, 'wm_por.png')
MASK_LND = os.path.join(DIR, 'wm_lnd.png')

def main():
    fn = sys.argv[1]

    img = cv.imread(fn)
    if img is None:
        print('Failed to load image file:', fn)
        sys.exit(1)

    dim = img.shape[:2]
    if dim == (3456, 4608):
        MASK = MASK_LND
    elif dim == (4608, 3456):
        MASK = MASK_POR
    else:
        raise ValueError('Nonstandard dimensions of the image: %s' % dim)

    img_mark = img.copy()
    mark = cv.imread(MASK, cv.IMREAD_GRAYSCALE)
    mark = cv.dilate(mark, np.array([[1,1,1]]*3), iterations=10)

    res = cv.inpaint(img_mark, mark, 3, cv.INPAINT_TELEA)

    # orig_f_split = os.path.splitext(fn)
    # orig_f = orig_f_split[0] + '-marked' + orig_f_split[1]
    # os.replace(fn, orig_f)
    os.unlink(fn)
    cv.imwrite(fn, res)


if __name__ == '__main__':
    main()
